package com.mycompany.app;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mycompany.app.Users;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Scanner;

public class Action {
    public static void main(String[] args) {
        String url = "https://reqres.in/api/users?page=2";
        Scanner scanner = new Scanner(System.in);
        int memo = 0;

        while (true) {
            System.out.println("Hello! Please enter the userId(from 1~12) or ENTER OTHERS TO EXIT");

            try {
                memo = scanner.nextInt();

                URI uri = URI.create(url + "&id=" + memo);

                HttpRequest request = HttpRequest.newBuilder(uri).build();

                String body = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString()).body();

                JsonParser parser = new JsonParser();
                JsonObject jsonObject = parser.parse(body).getAsJsonObject();

                JsonObject data = jsonObject.getAsJsonObject("data");

                Users user = new Gson().fromJson(data, Users.class);
                System.out.print("Your name: " + user.getFirstName() + ", " + user.getLastName());
                System.out.println(". And your email: " + user.getEmail());

                System.out.println("==================");
                System.out.println();
            } catch (Exception e) {
//                System.out.println(e);
                System.out.println("Have a nice day");
                break;
            }
        }
    }

}
